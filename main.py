#!/usr/bin/python

from Task import Task
from TaskManager import TaskManager
from TaskManagerApplication import TaskManagerApplication
import time

if __name__ == '__main__':

	"""
	t = Task("nowy task")
	print t.spent_time
	print t.name

	print "started"
	t.start_task()
	time.sleep(1)
	t.stop_task()
	print "ended"
	print t.spent_time
	
	print "started"
	t.start_task()
	time.sleep(2)
	t.stop_task()
	print "ended"
	print t.spent_time
	"""

	"""	
	tm = TaskManager('work')
	print '{} -> {}'.format(tm.get_task_name(0), tm.get_task_spent_time(0))
	print '{} -> {}'.format(tm.get_task_name(1), tm.get_task_spent_time(1))
	print '{} -> {}'.format(tm.get_task_name(2), tm.get_task_spent_time(2))

	tm.start_task(1)
	time.sleep(2)
	tm.stop_task(1)

	print '{} -> {}'.format(tm.get_task_name(0), tm.get_task_spent_time(0))
	print '{} -> {}'.format(tm.get_task_name(1), tm.get_task_spent_time(1))
	print '{} -> {}'.format(tm.get_task_name(2), tm.get_task_spent_time(2))
	
	tm.save_tasks_to_json()
	"""

	"""
	tm = TaskManager('brothers_birthday')
	tm.add_task('think about the present')

	print '{} -> {}'.format(tm.get_task_name(0), tm.get_task_spent_time(0))
	
	tm.start_task(0)
	time.sleep(2)
	tm.stop_task(0)

	print '{} -> {}'.format(tm.get_task_name(0), tm.get_task_spent_time(0))
	
	tm.save_tasks_to_json()
	"""

	"""
	tma = TaskManagerApplication()
	print tma.get_created_managers()
	for manager in tma.get_created_managers():
		tma.start_task_manager(manager)
		tma.end_task_manager()
	"""

	TaskManagerApplication()
	
