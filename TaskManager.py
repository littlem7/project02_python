from Task import Task
import json
from task_exceptions import *

import sys
from inspect import getfile as inspect_path
from os import path
import datetime
from datetime import timedelta
import io

class TaskManager(object):

	def __init__(self, task_manager_name):
		self.task_manager_name = task_manager_name

		self.time_string_tmpl = "%H:%M:%S.%f"
		
		class_path = inspect_path(self.__class__)
		self.config_json_name = 'TaskManager_{}.json'.format(self.task_manager_name)
		self.config_json_file = path.join(path.dirname(class_path), 'task_managers_configs', self.config_json_name)
		try:
			with open(self.config_json_file) as data_file:
				self.config_json = json.load(data_file)
		except (IOError, ValueError) as err:
			self.config_json = {"tasks": []}

		# creating tasks from json file
		self.task_list = []
		for task in self.config_json['tasks']:
			new_task_idx = self.add_task(task['name'])
			self.task_list[new_task_idx].spent_time = self.string_to_timedelta(task['spent_time'])

	def string_to_timedelta(self, td_string):
		time = datetime.datetime.strptime(td_string, self.time_string_tmpl)
		return datetime.timedelta(hours=time.hour, minutes=time.minute, seconds=time.second, microseconds=time.microsecond)
		
	def timedelta_to_string(self, get_timedelta):
		get_timedelta = get_timedelta + timedelta(microseconds=1)
		return str(get_timedelta)

	def add_task(self, name):
		self.check_if_task_exist(name)
		new_task = Task(name)
		self.task_list.append(new_task)
		return len(self.task_list) - 1

	def check_if_task_exist(self, name):
		for task in self.task_list:
			if task.name == name:
				raise InvalidTaskNameException('task already exist')

	def remove_task(self, idx):
		self.task_list.remove(self.task_list.get(idx))

	def get_task_name(self, idx):
		return self.task_list[idx].name

	def get_task_spent_time(self, idx):
		return self.task_list[idx].spent_time

	def get_task_status(self, idx):
		return self.task_list[idx].running

	def start_task(self, idx):
		self.task_list[idx].start_task()

	def stop_task(self, idx):
		self.task_list[idx].stop_task()

	def stop_all_tasks(self):
		for task in self.task_list:
			if task.running:
				task.stop_task()

	def num_of_tasks(self):
		return len(self.task_list)

	def save_tasks_to_json(self):
		self.config_json['tasks'] = []
		for task in self.task_list:
			self.config_json['tasks'].append({
				'name': task.name,
				'spent_time': self.timedelta_to_string(task.spent_time)
			})

		with open(self.config_json_file, 'w') as f:
			json.dump(self.config_json, f)
