from datetime import datetime, timedelta
from task_exceptions import *

class Task(object):

	def __init__(self, name):
		self.name = name
		self.spent_time = timedelta(hours=0, minutes=0, seconds=0, microseconds=0)
		self.running = False

	def stop_task(self):
		if not self.running:
			raise AlreadyStoppedTaskException('Task: {} already stopped'.format(self.name))
		else:
			self.running = False
			end_date = datetime.now()
			diff_date = end_date - self.start_date
			self.spent_time = self.spent_time + diff_date

	def start_task(self):
		if self.running:
			raise AlreadyRunningTaskException('Task: {} alread running from {}'.format(self.name, self.start_date))
		else:
			self.start_date = datetime.now()
			self.running = True



		