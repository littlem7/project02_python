from inspect import getfile as inspect_path
import os
import glob
from os import path
from TaskManager import TaskManager
import easygui as eg

class TaskManagerApplication(object):

	def __init__(self):
		self.tm = None
		self.gui_title = 'TaskManager'
		self.choose_manager()

	def get_created_managers(self):
		class_path = inspect_path(self.__class__)
		self.config_json_files_dir = path.join(path.dirname(class_path), 'task_managers_configs')
		
		os.chdir(self.config_json_files_dir)
		available_list = []
		for file in glob.glob("TaskManager_*.json"):
			raw_end_name = file.replace('TaskManager_', '')
			available_list.append(raw_end_name[:len(raw_end_name)-5])
		return available_list

	def start_task_manager(self, task_manager_name):
		if not self.tm:
			self.tm = TaskManager(task_manager_name)

	def end_task_manager(self):
		if self.tm:
			self.tm.stop_all_tasks()
			self.tm.save_tasks_to_json()
			self.tm = None

	def choose_manager(self):
		msg ="Choose your task manager"
		choices = self.get_created_managers()
		choices.append('new TaskManager')
		choice = eg.choicebox(msg, self.gui_title, choices)
		if choice in choices:
			if choice == 'new TaskManager':
				choice = self.set_name_for_new_task_manager()
			self.launch_task_manager(choice)
		else:
			print "Thank you for making this little TaskManager very happy!"

	def close_with_ask(self):
		msg = "Do you want to close this TaskManager and switch to another?"
		if eg.ccbox(msg, self.gui_title):
			self.end_task_manager()
			self.choose_manager()
		else:
			self.launch_task_manager()

	def manipulate_existing_task(self, task_ind):
		msg = "What do you want to do with {}".format(self.tm.get_task_name(task_ind))
		if self.tm.get_task_status(task_ind):
			choices = ["Stop task"]
		else:
			choices = ["Remove task","Start task"]
		choice = eg.buttonbox(msg, self.gui_title, choices=choices)
		if choice == "Stop task":
			self.tm.stop_task(task_ind)
		elif choice == "Start task":
			self.tm.start_task(task_ind)
		elif choice == "Remove task":
			self.tm.remove_task(task_ind)
		self.launch_task_manager()

	def add_new_task(self):
		msg = "Set name for new task"
		name = eg.multenterbox(msg,self.gui_title, ['name'])

		if name:
			self.tm.add_task(name[0])
		self.launch_task_manager()

	def launch_task_manager(self, task_manager_name = ''):
		self.start_task_manager(task_manager_name)

		msg ="Set which task should be executed"
		choices = ['ADD NEW TASK']
		# get all tasks
		for i in range(self.tm.num_of_tasks()):
			if self.tm.get_task_status(i):
				task_status = 'RUNNING'
			else:
				task_status = '-'
			choices.append('[{}] NAME: {} SPENT TIME: {} [{}]'.format(i, self.tm.get_task_name(i), self.tm.get_task_spent_time(i), task_status))

		choice = eg.choicebox(msg, self.gui_title, choices)

		if not choice:
			self.close_with_ask()
		else:
			if choice == 'ADD NEW TASK':
				self.add_new_task()
			else:
				raw_ind = choice.split(' ')[0]
				try:
					task_ind = int(raw_ind[1:len(raw_ind)-1])
					self.manipulate_existing_task(task_ind)
				except ValueError:
					self.launch_task_manager()
				


	def set_name_for_new_task_manager(self):
		msg = "Set name for new task manager"
		name = eg.multenterbox(msg,self.gui_title, ['responsibility of task manager'])

		if name:
			return name[0]
		else:
			self.choose_manager()
